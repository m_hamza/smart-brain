import React from 'react';
import './FaceRecognition.css'
const FaceRecognition = ({boxValues, imageURL}) =>{
    return(
        <div className='center ma'>
            <div className='mt2 absolute'>
                <img id='inputImage' src = {imageURL} alt="" width='500px' height='auto'/>
                <div className='bounding-box' style={{top:boxValues.top, right:boxValues.right, bottom:boxValues.bottom, left:boxValues.left}}></div>
            </div>
        </div>
    );
}
export default FaceRecognition;