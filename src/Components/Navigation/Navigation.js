import React from 'react';
const Navigation = ({onRoute}) =>{
    return(
        <nav style={{display:'flex', justifyContent:'flex-end'}}>
            <p onClick={()=>{
            					console.log("click","signIn");
            					onRoute('SignIn');
            				}
            			} 
            	className='f3 link dim black underline pa3 pointer'>
            	Sign Out
            </p>
        </nav>
    );
}
export default Navigation;