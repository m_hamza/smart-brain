import React from 'react';

class SignIn extends React.Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            password:''
        }
    }
    onEmailChange=(event)=>{
        this.setState({email:event.target.value});
    }
    onPasswordChange=(event)=>{
        this.setState({password:event.target.value});
    }
    onSubmitSignIn=(e)=>{
        e.preventDefault();
        fetch('http://localhost:3001/signIn',{
            method:'POST',
            headers:{'Content-Type':'application/json'},
            body:JSON.stringify({
                email:this.state.email,
                password:this.state.password
            })
        })
        .then(resp=>resp.json())
        .then(data=>{
            if(data === 'success')
            {
                console.log("response",data);
                this.props.onRoute('home');
            }
        })
        .catch(err=>{console.log("error in fetch",err)});
        // this.props.onRoute("home");
    }
    render(){
        return(
            <article className="br3 ba black-10 mv4 w-100 w-50-m w-25-l mw6 shadow-5 center">
            <main className="pa4 black-80">
                <form className="measure">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f1 fw6 ph0 mh0">Sign Up</legend>
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                            <input onChange={this.onEmailChange} className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="email" name="email-address"  id="email-address" />
                        </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                            <input onChange={this.onPasswordChange} className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="password" name="password"  id="password" />
                        </div>
                    </fieldset>
                    <div className="">
                    <input 
                    className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" 
                    type="submit" 
                    value="Sign In" 
                    onClick={this.onSubmitSignIn}
                    />
                    </div>
                    <div className="lh-copy mt3">
                        <a onClick={()=>this.props.onRoute('Register')} href="#0" className="f6 link dim black db">Register</a>
                    </div>
                </form>
            </main>
            </article>
        );
    }
}
export default SignIn;