import React from 'react';
import Tilt from 'react-tilt';
import './Logo.css';
const Logo = () =>{
    return(
        <div className='ma4 mt0 '>
            <Tilt className=" Tilt br2 shadow-2" options={{ max : 150 }} style={{ height: 150, width: 150 }} >
                <div className="Tilt-inner pd-3 tc" style={{padding:'20%',fontSize:'80px'}}> 👽 </div>
            </Tilt>
        </div>
    );
}

export default Logo;