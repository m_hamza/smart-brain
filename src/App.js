import React,{Component} from 'react';
import Navigation from './Components/Navigation/Navigation.js';
import Logo from './Components/Logo/Logo';
import ImageLinkForm from './Components/ImageLinkForm/ImageLinkForm';
import Rank from './Components/Rank/Rank.js';
import './App.css';
import Clarifai from 'clarifai';
import Particles from 'react-particles-js';
import FaceRecognition from './Components/FaceRecognition/FaceRecognition'
import SignIn from './Components/SignIn/SingIn';
import Register from './Components/Register/Register';

const particlesOptions={
  particles:{
    number:{
      value:80,
      density:{
        enable:true,
        value_area:800,
      }
    }
  }
}

const app=new Clarifai.App({
  apiKey:'ef8235485bc54287a708d3bce6543005'
});

class App extends Component
{
  constructor()
  {
    super();
    this.state={
      input:'',
      imageURL:'',
      box:{},
      route:'SignIn',
      user:{
        id:'',
        name:'',
        email:'',
        entries:0,
        joined:''
      }
    }
  }
  onInputChange=(event)=>{
    this.setState({input:event.target.value});
  }
  
  loadUser=(data)=>{
    console.log(data);
    this.setState({user:{
        id:data.id,
        name:data.name,
        email:data.email,
        entries:data.entries,
        joined:data.joined
    }})
  }
  calculatebox=(data)=>{
    const image = document.getElementById('inputImage');
    const width=Number(image.width);
    const height = Number(image.height);
    const results = data.outputs[0].data.regions[0].region_info.bounding_box;
    return {
      top:results.top_row*height,
      left:results.left_col*width,
      bottom:height-(results.bottom_row*height),
      right:width-(results.right_col*width)
    }
  }

  displaybox=(calculatedBox)=>{
    this.setState({box:calculatedBox});
  }

  onButtonClick=()=>{
    this.setState({imageURL:this.state.input})
    app.models.predict(Clarifai.FACE_DETECT_MODEL, this.state.input)
    .then(response=>{
      if(response)
      {
        fetch('http://localhost:3001/image',{
          method:'put',
          headers:{'Content-Type':'application/json'},
          body:JSON.stringify({
            id:this.state.id
          })
        }).then(resp=>resp.json())
        .then(count=>this.setState(Object.assign(this.state.user,{entries:count})))
      }
      this.displaybox(this.calculatebox(response))
    })
    .catch(err=>console.log(err));
  }

  onRoute=(route)=>{
    console.log("route",route);
    this.setState({route:route});
  }
  render()
  {
    if(this.state.route==="SignIn")
    {
      console.log("SignIn");
      return(
        <div className="App">
          <Particles params={particlesOptions} className="particles"/>
          <SignIn onRoute={this.onRoute}/>
        </div>
      );
    }
    else if(this.state.route==="Register")
    {
      console.log("Register");
      return(
        <div className="App">
          <Particles params={particlesOptions} className="particles"/>
          <Register loadUser={this.loadUser} onRoute={this.onRoute}/>
        </div>
      );
    }
    else
    {
      console.log("home");
      return(<div className="App">
          <Particles params={particlesOptions} className="particles"/>
          <div>
            <Navigation onRoute={this.onRoute}/>
            <Logo />
            <Rank name={this.state.user.name} rank={this.state.user.entries}/>
            <ImageLinkForm onInputChange={this.onInputChange} onSubmit={this.onButtonClick}/>
            <FaceRecognition boxValues={this.state.box} imageURL={this.state.imageURL}/>
          </div>
        </div>);
    }
    // return (
        
    //          this.state.route ==='SignIn'?():(
    //             this.state.route==="Register"?():(
                  
    //             )
    //           )
    //         //   
    //         //   
    //         // )

    //       }
    //     </div>
    // );
  }
}

export default App;
